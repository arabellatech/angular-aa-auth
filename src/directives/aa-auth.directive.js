(function () {
    'use strict';

    angular.module('aaAuth')
            .directive('aaAuth', aaAuthDirective);

    function aaAuthDirective() {
        return {
            link: link,
            restrict: 'A'
        };

        function link(scope, elem, attrs) {
            //once Angular is started, remove class:
            elem.removeClass('hide');

//            var login = elem.find('#login-holder');
//            var main = elem.find('#content');
//
//            login.hide();
//
//            scope.$on('event:auth-loginRequired', function () {
//                login.slideDown('slow', function () {
//                    main.hide();
//                });
//            });
//            scope.$on('event:auth-loginConfirmed', function () {
//                main.show();
//                login.slideUp();
//            });
        }
    }
}());
