(function () {
    'use strict';

    angular.module('aaAuth', ['ngResource']);
}());

(function () {
    'use strict';

    angular.module('aaAuth')
            .directive('aaAuthLoginForm', aaAuthLoginFormDirective);

    function aaAuthLoginFormDirective() {
        return {
            link: link,
            restrict: 'A'
        };

        function link(scope, elem, attrs) {
        }
    }
}());

(function () {
    'use strict';

    angular.module('aaAuth')
            .directive('aaAuth', aaAuthDirective);

    function aaAuthDirective() {
        return {
            link: link,
            restrict: 'A'
        };

        function link(scope, elem, attrs) {
            //once Angular is started, remove class:
            elem.removeClass('hide');

//            var login = elem.find('#login-holder');
//            var main = elem.find('#content');
//
//            login.hide();
//
//            scope.$on('event:auth-loginRequired', function () {
//                login.slideDown('slow', function () {
//                    main.hide();
//                });
//            });
//            scope.$on('event:auth-loginConfirmed', function () {
//                main.show();
//                login.slideUp();
//            });
        }
    }
}());

(function () {
    'use strict';

    angular.module('aaAuth')
            .factory('loginResource', loginResource);

    loginResource.$inject = ['$resource'];
    function loginResource($resource) {

        var url = '/api/accounts/auth';

        var params = {
            pk: '@pk'
        };

        var actions = {
            post: {
                method: 'POST',
            }
        };

        return $resource(url, params, actions);
    }

}());

(function () {
    'use strict';

    angular.module('aaAuth')
            .factory('logoutResource', logoutResource);

    logoutResource.$inject = ['$resource'];
    
    function logoutResource($resource) {

        var url = Settings.apiDealDocumentsUrl;

        var params = {};

        var actions = {
            get: {
                method: 'POST',
//                transformResponse: function transformResponse(data, headersGetter) {
//                    var json = angular.fromJson(data);
//                    return new dealDocumentModel(json);
//                },
//                interceptor: {
//                    response: function (response) {
//                        var deferred = $q.defer();
//
//                        deferred.resolve(response.data);
//
//                        return deferred.promise;
//                    }
//                }
            }
        };

        return $resource(url, params, actions);
    }

}());

(function () {
    'use strict';

    angular.module('aaAuth')
            .factory('aaAuthService', aaAuthService);

    aaAuthService.$inject = ['loginResource', 'logoutResource'];
    
    function aaAuthService(loginResource, logoutResource) {
        var _user;

        var service = {
            login: login,
            logout: logout,
            isLoggedIn: isLoggedIn,
            currentUser: currentUser
        };

        return service;

        function login(username, password) {
            var params = {
                email: username,
                password: password
            };
            
            var promise = loginResource.post(params).$promise;
            
            promise.then(successCallback, errorCallback);
            
            return promise;
            
            function successCallback(data) {
                _user = data;
            }
            
            function errorCallback() {
                clear();
            }
        }

        function logout() {
            var params = {};
            
            var promise = logoutResource.post(params).$promise;
            
            promise.then(successCallback, errorCallback);
            
            return promise;
            
            function successCallback() {
                clear();
            }
            
            function errorCallback() {
                clear();
            }
        }

        function isLoggedIn() {
            return false;
        }

        function currentUser() {
            return _user;
        }
        
        function clear() {
            _user = null;
        }
    }

}());
